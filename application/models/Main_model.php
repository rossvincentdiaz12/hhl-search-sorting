<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Main_model extends CI_Model {

    public function __construct() {
		parent::__construct(); 
	}

	//displaying coupons per month
	public function getSearch(){			
		$this->db->select('*');
		$this->db->from('search_history');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}		
	}

	//displaying coupons per month
	public function getSearchbydate($date){			
		$this->db->select('*');
		$this->db->from('search_history');
		$this->db->where('date',$date);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}		
	}

	
	//displaying coupons per month
	public function getSearchbydateRange($date,$date2){			
		$this->db->select('*');
		$this->db->from('search_history');
		$this->db->where('date BETWEEN "'.$date.'" and "'.$date2.'"');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}		
	}
	
	public function get_sql($sql)
	{ 
		$query = $this->db->query($sql); 
		return $query->result();
		
	}

	

}