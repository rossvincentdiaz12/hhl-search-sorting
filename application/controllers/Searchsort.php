<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Searchsort extends CI_Controller{

    public function __construct(){
        
        parent::__construct();
        $this->load->helper('url');
        // Load session
         $this->load->library('Session');
        // Load model 
        $this->load->model('Main_model');        
     }

    //list for coupons 
    public function index(){

        // Load view
        $data['sort'] = $this->Main_model->getSearch();
        $this->load->view('index',$data);  
    }
       //list for coupons 
    public function sortdate(){

        // Load view
        $data['sort'] = $this->Main_model->getSearchbydate($_GET['sort']);
        $this->load->view('sorted',$data);  
    }

           //list for coupons 
    public function range(){

        // Load view
        $data['sort'] = $this->Main_model->getSearchbydateRange($_GET['sort'],$_GET['sort2']);
        $this->load->view('mostsearch',$data);  
    }
        
}