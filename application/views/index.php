<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Search History</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href='<?= base_url() ?>assets/bootstrap-star-rating/css/star-rating.min.css' type='text/css' rel='stylesheet'>
	<script src='<?= base_url() ?>assets/js/jquery-3.3.1.js' type='text/javascript'></script>
	<script src='<?= base_url() ?>assets/bootstrap-star-rating/js/star-rating.min.js' type='text/javascript'></script>	
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/img.js"></script>
	<link href="<?= base_url() ?>assets/post_view.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/footable.core.css" />
	<script src="assets/footable.js"></script>
	<script src="assets/footable.filter.js"></script>
	<script src="assets/footable.paginate.js"></script>
</head>
	<body>
  <div class='content'>
        <form action="searchsort/sortdate">
          <label for="date">Date:</label>
          <input type="date" id="sort" name="sort" required >
          <input type="submit">
        </form>

        <table class="table demo default footable" data-filter="#filter" data-filter-text-only="false" data-page-size="20" data-page-navigation=".pagination">
              <thead>
                  <tr>			
                      <th>ID</th>
                      <th>IP Address</th>
                      <th>Search Value</th>
                      <th>Date</th>
                      
                  </tr>
              </thead>
              <tbody>
              <?php 
                  if($sort){
                      foreach($sort as $sorts){
              ?>
                  <tr>					
                      <td><?php echo $sorts->id; ?></td>
                      <td><?php echo $sorts->ip_address; ?></td>
                      <td><?php echo $sorts->search_value; ?></td>
                      <td><?php echo $sorts->date; ?></td>
                
                  </tr>
              <?php
                      }
                  }
              ?>
              </tbody>
              <!-- paganate pages -->
              <tfoot class="hide-if-no-paging">				
                      <tr>
                          <td colspan="5" class="text-center">
                            <ul class="pagination"></ul>
                          </td>
                      </tr>
              </tfoot>                                   
        </table>			
  </div>

	<!-- Pagitation Script -->
<script type="text/javascript">
    (function($) {
		// Filter the content into the table
      window.footable.options.filter.filterFunction = function(index) {
        var $t = $(this),
          $table = $t.parents('table:first'),
          filter = $table.data('current-filter').toUpperCase(),
          columns = $t.find('td');

        var regEx = new RegExp("\\b" + filter + "\\b");
        var result = false;
        for (i = 0; i < columns.length; i++) {
          var text = $(columns[i]).text();
          result = regEx.test(text.toUpperCase());
          if (result === true)
            break;


          if (!$table.data('filter-text-only')) {
            text = $(columns[i]).data("value");
            if (text)
              result = regEx.test(text.toString().toUpperCase());
          }

          if (result === true)
            break;
        }

        return result;
      };
	  // set pagitation pages
      $('table').footable().bind('footable_filtering', function(e) {
        var selected = $('.filter-status').find(':selected').text();
        if (selected && selected.length > 0) {
          e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
          e.clear = !e.filter;
        }
      }); 
    })(jQuery);
  </script>

	</body>
</html>