/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.24 : Database - search_history
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`search_history` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `search_history`;

/*Table structure for table `search_history` */

DROP TABLE IF EXISTS `search_history`;

CREATE TABLE `search_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` text NOT NULL,
  `search_value` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=862 DEFAULT CHARSET=latin1;

/*Data for the table `search_history` */

insert  into `search_history`(`id`,`ip_address`,`search_value`,`date`) values 
(861,'75.13.141.177','Gut Alive','2020-09-15'),
(852,'68.8.237.208','members checkout page','2020-09-14'),
(853,'152.32.96.192','ProbioShield','2020-09-14'),
(854,'76.226.73.222','gout','2020-09-14'),
(855,'47.219.105.15','uri alive','2020-09-14'),
(856,'49.150.39.64','Vision Alive 30 Capsules','2020-09-14'),
(857,'47.219.105.15','Uri Alive','2020-09-14'),
(858,'65.78.77.35',' Gut Alive','2020-09-14'),
(859,'155.186.179.98','Vision Alive Max','2020-09-14'),
(860,'75.13.141.177','gut alive','2020-09-14'),
(840,'174.57.194.242','Joints alive','2020-09-13'),
(841,'174.57.194.242','JointsAlive','2020-09-13'),
(842,'67.167.46.182','uri=Alive','2020-09-13'),
(843,'67.167.46.182','Uri- Alive','2020-09-13'),
(844,'67.167.46.182','urinary ','2020-09-13'),
(845,'152.32.109.191','osteo alive','2020-09-13'),
(846,'104.220.140.159','Gut Alive','2020-09-13'),
(847,'104.220.140.159','Myco Ultra','2020-09-13'),
(848,'98.207.226.216','joints  alive','2020-09-16'),
(849,'68.132.9.109','My order','2020-09-13'),
(850,'76.168.236.105','create an account','2020-09-13'),
(851,'24.23.28.78','joint alive','2020-09-13'),
(826,'69.221.228.5','Gut Alive','2020-09-12'),
(827,'70.106.20.25','biox 4','2020-09-12'),
(828,'68.225.253.30','stop the meltdown of your eyes','2020-09-12');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
